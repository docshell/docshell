# docshell

[https://www.docshell.cloud](https://www.docshell.cloud)

## What is docshell?

Docshell is the new way to document, comment, reuse and share your sysadmin activities. It consists of a set of tools built around an xsd schema that defines a command line activity. 

## Projects

### docshell-schema
xsd schema defining a docshell

https://gitlab.com/docshell/docshell-schema

### docshell-cli
Command line interface to generate docshell from shell scripts

https://gitlab.com/docshell/docshell-cli

